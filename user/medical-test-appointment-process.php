<?php

require_once("../config.php");

//d($_POST);

$name = $_POST['name'];
$test = $_POST['title'];
$phone = $_POST['phone'];
$gender = $_POST['gender'];
$city = $_POST['city'];
$state = $_POST['state'];
$zip = $_POST['zip'];
$message = $_POST['msg'];
$status = "Pending";
$color = "badge-secondary";

$address = $city."-".$zip.", ".$state;

$appoint_req = [
        "id" => uniqid(),
        "patient name" => $name,
        "test name" => $test,
        "gender" => $gender,
        
        "address" => $address,
        "phone"  => $phone,
        "message" => $message,
        "status" => $status,
        "status color" => $color
];

//dd($appoint_req);

$admin_appoint_req_json =  file_get_contents($json."medical-test-appoint.json");
$arr_admin_appoint_req = json_decode($admin_appoint_req_json, "true");

$arr_admin_appoint_req[] = $appoint_req;
$admin_appoint_req_json = json_encode($arr_admin_appoint_req);

if(file_exists($json."medical-test-appoint.json"))
{
    $result = file_put_contents($json."medical-test-appoint.json", $admin_appoint_req_json);
}
else{
    echo "Not Found!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3">Medical Appointment request has been sent Successfully.</p>
								<a href="medical-test-user.php" class="btn bg-success-400">GO BACK</a>
							</div>
						</div>
    
</body>
</html>

