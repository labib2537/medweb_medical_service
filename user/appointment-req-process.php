<?php

require_once("../config.php");

//dd($_POST);

$name = $_POST['name'];
$doctor = $_POST['doc'];
$phone = $_POST['phone'];
$gender = $_POST['gender'];
$city = $_POST['city'];
$state = $_POST['state'];
$zip = $_POST['zip'];
$message = $_POST['msg'];

$address = $city."-".$zip.", ".$state;

$appoint_req = [
        "id" => uniqid(),
        "patient name" => $name,
        "doctor name" => $doctor,
        "gender" => $gender,
        // "city" => $city,
        // "state" => $state,
        // "zip" => $zip,
        "address" => $address,
        "phone"  => $phone,
        "message" => $message,
        "status" => "Pending",
        "status color" => "badge-secondary"
];

//dd($appoint_req);

$admin_appoint_req_json =  file_get_contents($json."admin-appointment-req.json");
$arr_admin_appoint_req = json_decode($admin_appoint_req_json, "true");

$arr_admin_appoint_req[] = $appoint_req;
$admin_appoint_req_json = json_encode($arr_admin_appoint_req);

if(file_exists($json."admin-appointment-req.json"))
{
    $result = file_put_contents($json."admin-appointment-req.json", $admin_appoint_req_json);
}
else{
    echo "Not Found!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3">Appointment request has been sent Successfully.</p>
								<a href="user_appointment.php" class="btn bg-success-400">GO BACK</a>
							</div>
						</div>
    
</body>
</html>

