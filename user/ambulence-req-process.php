<?php

require_once("../config.php");

$name = $_POST['name'];
$phone = $_POST['phone'];
$address = $_POST['add'];
$message = $_POST['msg'];
$status = "Pending";
$sta_col = "badge-secondary";

$ambu_req = [
    "id" => uniqid(),
    "patient name" => $name,
    "address" => $address,
    "phone" => $phone,
    "message" => $message,
    "status" => $status,
    "status color" => $sta_col
];


$admin_ambulence_req_json =  file_get_contents($json."admin-ambulence-req.json");
$arr_admin_ambulence_req = json_decode($admin_ambulence_req_json, "true");

$arr_admin_ambulence_req[] = $ambu_req;
$admin_ambulence_req_json = json_encode($arr_admin_ambulence_req);

if(file_exists($json."admin-ambulence-req.json"))
{
    $result = file_put_contents($json."admin-ambulence-req.json", $admin_ambulence_req_json);
}
else{
    echo "Not Found!";
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3">Ambulence request has been sent Successfully.</p>
								<a href="ambulence-request-user.php" class="btn bg-success-400">GO BACK</a>
							</div>
						</div>
    
</body>
</html>
