<?php

require_once("../config.php");

//dd($_POST);

$name = $_POST['name'];
$email = $_POST['email'];
$question = $_POST['qus'];


$appoint_req = [
        "id" => uniqid(),
        "name" => $name,
        "email" => $email,
        "question" => $question
];

//dd($appoint_req);

$admin_appoint_req_json =  file_get_contents($json."faq-req.json");
$arr_admin_appoint_req = json_decode($admin_appoint_req_json, "true");

$arr_admin_appoint_req[] = $appoint_req;
$admin_appoint_req_json = json_encode($arr_admin_appoint_req);

if(file_exists($json."faq-req.json"))
{
    $result = file_put_contents($json."faq-req.json", $admin_appoint_req_json);
}
else{
    echo "Not Found!";
}

if($result)
{
    $message = "Your Question has been sent successfully.";
    set_session('message',$message);
    redirect('user-faq.php');
}

