<?php

require_once("../config.php");


$name = $_POST['name'];
$message = $_POST['message'];
$email = $_POST['email'];
$phone = $_POST['phone'];

$contact_message = [
    "id" => uniqid(),
    "name" => $name,
    "email" => $email,
    "phone" => $phone,
    "message" => $message
];



//dd($contact_message);

$messageData = file_get_contents($json."contact-message.json");
$arr_message = json_decode($messageData, "true");

$arr_message[] = $contact_message;

$messageData = json_encode($arr_message);

if(file_exists($json."contact-message.json"))
{
    $result = file_put_contents($json."contact-message.json", $messageData);


    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <?php include_once($short.'head.php'); ?>
        <title>Successful</title>
    </head>
    <body>
    
    <div class="card">
                                <div class="card-body text-center">
                                <i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                    <h5 class="card-title">Successful</h5>
                                    <p class="mb-3">Your mesage has been sent Successfully.</p>
                                    <a href="../index.php" class="btn bg-success-400">GO BACK</a>
                                </div>
                            </div>
        
    </body>
    </html>
    <?php
    }
    else{
        echo "Not Found!";
    }
    
    ?>