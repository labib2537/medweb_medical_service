<?php
require_once("../config.php");

//dd($_GET);



// store the data from user to json

$name = $_POST['name'];
$age = $_POST['age'];
$address = $_POST['add'];
$phone = $_POST['phone'];
$date = $_POST['date'];
$room = $_POST['room'];
$bill = "";
$status = "Admitted";
$sta_color = "badge-success";

$entry = [
    'id' => uniqid(),
    'patient name' =>$name,
    'age' =>$age,
    'address' =>$address,
    'phone' =>$phone,
    'date' =>$date,
    'room' =>$room,
    'bill' => $bill,
    'status' => $status,
    'status color' => $sta_color
];

//dd($entry);

$admin_patient_list_json =  file_get_contents($json."admin-patient-list.json");
$arr_admin_patient_list = json_decode($admin_patient_list_json);

$arr_admin_patient_list[] = (object)$entry;
$admin_patient_list_json = json_encode($arr_admin_patient_list);
//dd($admin_patient_list_json);

if(file_exists($json."admin-patient-list.json"))
{
    $result = file_put_contents($json."admin-patient-list.json",$admin_patient_list_json);
    echo "";
}
else{
    echo "Not Found!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3">Patient's information has been saved Successfully.</p>
								<a href="patient-list.php" class="btn bg-success-400">GO TO LIST</a>
							</div>
						</div>
    
</body>
</html>


