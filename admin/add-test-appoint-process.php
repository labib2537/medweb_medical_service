<?php

require_once("../config.php");

//dd($_POST);


$name = $_POST['name'];
$test = $_POST['test'];
$address = $_POST['add'];
$phone = $_POST['phone'];
$gender = $_POST['gender'];
$date = $_POST['date'];


$appointments = [

        
        "id" => uniqid(),
        "patient name" => $name,
        "test name" => $test,
        "gender" => $gender,
        "address" => $address,
        "phone" => $phone,
        "date" => $date

];

//dd($appointments);


$appoint_json =  file_get_contents($json."medicalTest-appoint-list.json");
$arr_appoint = json_decode($appoint_json, "true");

$arr_appoint[] = $appointments;

$appoint_json = json_encode($arr_appoint);

if(file_exists($json."medicalTest-appoint-list.json"))
{
    $result = file_put_contents($json."medicalTest-appoint-list.json", $appoint_json);
}
else{
    echo "Not Found!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3"> New Medical Test Appointment has been saved Successfully.</p>
								<a href="medicalTest-appoint-list.php" class="btn bg-success-400">GO TO LIST</a>
							</div>
						</div>
    
</body>
</html>
