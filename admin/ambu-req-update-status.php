<?php
require_once("../config.php");

//d($_POST);


// store the data from user to json
$uid = $_POST['id'];
$name = $_POST['name'];
$address = $_POST['address'];
$phone = $_POST['phone'];
$message = $_POST['message'];
$status = $_POST['status'];
$sta_color = $_POST['color'];


if($status == 'Pending' && $sta_color == 'badge-secondary')
{
    $sta_color = 'badge-success';
    $status = 'Assigned';
}



$entry = [
    'id' => $uid,
    'patient name' =>$name,
    'address' =>$address,
    'phone' =>$phone,
    'message' =>$message,
    'status' => $status,
    'status color' => $sta_color
];
//dd($entry);




$admin_patient_list_json =  file_get_contents($json."admin-ambulence-req.json");
$arr_admin_patient_list = json_decode($admin_patient_list_json);

foreach($arr_admin_patient_list as $key=>$patient)
{
    if($patient->id==$_POST['id'])
    {
        break;
    }
}
//d($key);
$arr_admin_patient_list[$key] = (object)$entry;
//dd($arr_admin_patient_list);
$admin_patient_list_json = json_encode($arr_admin_patient_list);
//dd($admin_patient_list_json);

if(file_exists($json."admin-ambulence-req.json"))
{
    $result = file_put_contents($json."admin-ambulence-req.json",$admin_patient_list_json);
}
else{
    echo "Not Found!";
}

if($result)
{
    $message = "Ambulance Request Status is updated successfully";
    set_session('message', $message);
    redirect('ambulence-req-check.php');
}



