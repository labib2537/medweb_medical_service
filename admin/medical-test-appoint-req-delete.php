<?php

require_once("../config.php");

//dd($_POST);


$appoint_json =  file_get_contents($json."medical-test-appoint.json");
$arr_appoint = json_decode($appoint_json, "true");

foreach($arr_appoint as $key=>$appoint)
{
    if($appoint['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_appoint, $key, 1);
$appoint_json = json_encode($arr_appoint);

if(file_exists($json."medical-test-appoint.json"))
{

    $result = file_put_contents($json."medical-test-appoint.json", $appoint_json);
    if($result)
    {
        redirect('medical-test-appointment-list.php');
    }

}
else
{
    echo "not Found!";
}
