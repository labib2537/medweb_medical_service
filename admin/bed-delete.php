<?php

require_once("../config.php");

//dd($_POST);


$admin_bed_list_json =  file_get_contents($json."admin-bed-list.json");
$arr_admin_bed_list = json_decode($admin_bed_list_json, "true");

foreach($arr_admin_bed_list as $key=>$bed)
{
    if($bed['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_admin_bed_list, $key, 1);
$admin_bed_list_json = json_encode($arr_admin_bed_list);

if(file_exists($json."admin-bed-list.json"))
{

    $result = file_put_contents($json."admin-bed-list.json", $admin_bed_list_json);
    if($result)
    {
        redirect('bed_allotment_list.php');
    }

}
else
{
    echo "not Found!";
}
