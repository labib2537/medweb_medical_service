<?php

require_once("../config.php");

$test_json =  file_get_contents($json."admin-test-report.json");
$arr_test = json_decode($test_json, "true");

foreach($arr_test as $key=>$test)
{
    if($test['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_test, $key, 1);
$test_json = json_encode($arr_test);

if(file_exists($json."admin-test-report.json"))
{

    $result = file_put_contents($json."admin-test-report.json", $test_json);
    if($result)
    {
        redirect('test-report-list.php');
    }

}
else
{
    echo "not Found!";
}