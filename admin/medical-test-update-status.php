<?php

require_once("../config.php");

//d($_POST);


$uid = $_POST['id'];
$name = $_POST['name'];
$test = $_POST['test'];
$address = $_POST['add'];
$phone = $_POST['phone'];
$message = $_POST['message'];
$gender = $_POST['gender'];
$status = $_POST['status'];
$sta_color = $_POST['color'];

if($status=='Pending' && $sta_color=='badge-secondary')
{
    $sta_color = 'badge-success';
    $status = 'Accepted';
}
else
{
    $sta_color = 'badge-secondary';
    $status = 'Pending';
}

$ambu_info = [
    "id" => $uid,
    "patient name" => $name,
    "test name" => $test,
    "gender" => $gender,
    "address" => $address,
    "phone" => $phone,
    "message" => $message,
    "status" => $status,
    "status color" => $sta_color 
];

//dd($ambu_info);
$admin_ambulence_info_json =  file_get_contents($json."medical-test-appoint.json");
$arr_admin_ambulence_info = json_decode($admin_ambulence_info_json, "true");

foreach($arr_admin_ambulence_info as $key=>$info)
{
    if($info['id']==$uid)
    {
        break;
    }
}


$arr_admin_ambulence_info[$key] = $ambu_info;
$admin_ambulence_info_json = json_encode($arr_admin_ambulence_info);

if(file_exists($json."medical-test-appoint.json"))
{
    $result = file_put_contents($json."medical-test-appoint.json", $admin_ambulence_info_json);
}
else
{
    echo "Not Found!";
}

if($result)
{
    $message = 'Medical Test Appointment Status is updated successfully';
    set_session('message',$message);
    redirect('medical-test-appointment-list.php');
}