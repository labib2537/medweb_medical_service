<?php
require_once("../config.php");

//dd($_POST);



// store the data from user to json
$uid = $_POST['id'];
$name = $_POST['name'];
$age = $_POST['age'];
$address = $_POST['add'];
$phone = $_POST['phone'];
$date = $_POST['date'];
$room = $_POST['room'];
$bill = $_POST['bill'];
$status = $_POST['status'];
$sta_color = $_POST['color'];

$entry = [
    'id' => $uid,
    'patient name' =>$name,
    'age' =>$age,
    'address' =>$address,
    'phone' =>$phone,
    'date' =>$date,
    'room' =>$room,
    'bill' => $bill,
    'status' => $status,
    'status color' => $sta_color
];

//dd($entry);

$admin_patient_list_json =  file_get_contents($json."admin-patient-list.json");
$arr_admin_patient_list = json_decode($admin_patient_list_json);

foreach($arr_admin_patient_list as $key=>$patient)
{
    if($patient->id==$_POST['id'])
    {
        break;
    }
}

$arr_admin_patient_list[$key] = (object)$entry;
$admin_patient_list_json = json_encode($arr_admin_patient_list);
//dd($admin_patient_list_json);

if(file_exists($json."admin-patient-list.json"))
{
    $result = file_put_contents($json."admin-patient-list.json",$admin_patient_list_json);
}
else{
    echo "Not Found!";
}

if($result)
{
    $message = "Patient Information is updated successfully";
    set_session('message', $message);
    redirect('patient-list.php');
}


