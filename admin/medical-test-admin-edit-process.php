<?php
require_once('../config.php');

//dd($_POST);
//image processing
$src = null;
$old_picture = null;
$new_picture = null;

$old_picture = $_POST['old_picture'];
if( array_key_exists('picture', $_FILES) && !empty($_FILES['picture']['name'])){
    $filename = $_FILES['picture']['name']; // if you want to keep the name as is
    $filename = uniqid()."_".$_FILES['picture']['name']; // if you want to keep the name as is
    $from = $_FILES['picture']['tmp_name'];
    $to = $uploads."test-images/".$filename;

    if(upload($from, $to)){
        $new_picture = $filename ;
    }

    if(file_exists($uploads."test-images/".$old_picture )){
        unlink( $uploads."test-images/".$old_picture );
    }
    
}

$uid = $_POST['id'];
$title = $_POST['title'];
$cost = $_POST['cost'];
$time = $_POST['time'];
$place = $_POST['place'];
$type = $_POST['type'];
$image = $new_picture ?? $old_picture;

$service = [
        "id" => $uid,
        "title" => $title,
        "cost"=> $cost,
        "time" => $time,
        "place"=> $place,
        "image"=> $image,
        "type"=> $type,
];

//d($service);

$service_items = file_get_contents($json."admin-medical-test.json");
$arr_service_items = json_decode($service_items, "true");

foreach($arr_service_items as $key=>$a_service)
{
    if($a_service['id']==$uid)
    {
        break;
    }
}

//d($key);
//dd($service);

$arr_service_items[$key] = $service;
$service_items = json_encode($arr_service_items);

if(file_exists($json."admin-medical-test.json"))
{
    $result = file_put_contents($json."admin-medical-test.json", $service_items);
}
else{
    echo "Not Found!";
}

if($result)
{
    $message = "Service Card information is updated Successfully";
    set_session('message',$message);
    redirect('medical-test-admin.php');
}