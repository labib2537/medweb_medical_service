<?php

require_once('../config.php');

//d($_POST);

$uid = $_POST['id'];
$name = $_POST['title'];
$head = $_POST['head'];
$content = $_POST['content'];
$icon = $_POST['icon'];

$service = [
        "id" => $uid,
        "name" => $name,
        "heading"=> $head,
        "paragraph" => $content,
        "icon"=> $icon
];

//d($service);

$service_items = file_get_contents($json."service-front.json");
$arr_service_items = json_decode($service_items, "true");

foreach($arr_service_items as $key=>$a_service)
{
    if($a_service['id']==$uid)
    {
        break;
    }
}

//d($key);
//dd($service);

$arr_service_items[$key] = $service;
$service_items = json_encode($arr_service_items);

if(file_exists($json."service-front.json"))
{
    $result = file_put_contents($json."service-front.json", $service_items);
}
else{
    echo "Not Found!";
}

if($result)
{
    $message = "Service Card information is updated Successfully";
    set_session('message',$message);
    redirect('service-list.php');
}