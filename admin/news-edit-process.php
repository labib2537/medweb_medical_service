<?php

require_once("../config.php");



//dd($_FILES);

$src = null;
$new_picture = null;
$old_picture = null;
$old_picture = $_POST['old_picture'];

if(array_key_exists('new_picture', $_FILES) && !empty($_FILES['new_picture']['name']))
{
    $filename = uniqid().'_'.$_FILES['new_picture']['name'];
    $from = $_FILES['new_picture']['tmp_name'];
    $to = $uploads.'news-images/'.$filename;
    if(upload($from, $to)){
        $new_picture = $filename;
    }
    if(file_exists($uploads.'news-images/'.$old_picture)){
        unlink($uploads.'news-images/'.$old_picture);
    }

}







$uid = $_POST['id'];
$name = $_POST['title'];
$src = $new_picture ?? $old_picture;
$alt = $_POST['alt'];
$head = $_POST['head'];
$para = $_POST['content'];
$date = $_POST['date'];





$news = [

    "id" => $uid,
    "name" => $name,
    "src" => $src,
    "alt" => $alt,
    "heading" => $head,
    "paragraph" => $para,
    "post time" => $date

];

//dd($news);

$news_items = file_get_contents($json."news-front.json");
$arr_news_items = json_decode($news_items, "true");

//dd($arr_news_items);

foreach($arr_news_items as $key=>$a_news)
{
    if($a_news['id']==$uid)
    {
        break;
    }
}


$arr_news_items[$key] = $news;
$news_items = json_encode($arr_news_items);

if(file_exists($json."news-front.json"))
{
    $result = file_put_contents($json."news-front.json", $news_items);

}else{
    echo "Not Found!";
}

if($result)
{
    $message = "News information is updated Successfully";
    set_session('message', $message);
    redirect('news-list-view.php');
}

?>
