<?php

require_once("../config.php");

//dd($_POST);

//dd($_FILES);

$filename = uniqid().'_'.$_FILES['pdf']['name'];
$from = $_FILES['pdf']['tmp_name'];
$to = $uploads.'test-reports/'.$filename;
$report = null;
if(upload($from,$to)){
    $report = $filename;
}


$name = $_POST['name'];
$address = $_POST['add'];
$phone = $_POST['phone'];
$test = $_POST['test'];
$date = $_POST['date'];
$file = $report;

$test_report = [


    "id" => uniqid(),
    "patient name" => $name,
    "test name" => $test,
    "address" => $address,
    "phone" => $phone,
    "date" => $date,
    "report file" => $file

];

//dd($test_report);

$test_json =  file_get_contents($json."admin-test-report.json");
$arr_test = json_decode($test_json, "true");

$arr_test[] = $test_report;
$test_json = json_encode($arr_test); 

if(file_exists($json."admin-test-report.json")){
    $result = file_put_contents($json."admin-test-report.json", $test_json);
}else{
    echo "Not Found!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3">Test Report has been saved Successfully.</p>
								<a href="test-report-list.php" class="btn bg-success-400">GO TO LIST</a>
							</div>
						</div>
    
</body>
</html>
