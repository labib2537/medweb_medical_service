<?php

require_once("../config.php");

//dd($_POST);


$admin_ambulence_req_json =  file_get_contents($json."admin-ambulence-req.json");
$arr_admin_ambulence_req = json_decode($admin_ambulence_req_json, "true");

foreach($arr_admin_ambulence_req as $key=>$ambu_req)
{
    if($ambu_req['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_admin_ambulence_req, $key, 1);
$admin_ambulence_req_json = json_encode($arr_admin_ambulence_req);

if(file_exists($json."admin-ambulence-req.json"))
{

    $result = file_put_contents($json."admin-ambulence-req.json", $admin_ambulence_req_json);
    if($result)
    {
        redirect('ambulence-req-check.php');
    }

}
else
{
    echo "Not Found!";
}
