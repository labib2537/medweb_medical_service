<?php

require_once("../config.php");

//dd($_POST);


$news_items = file_get_contents($json."news-front.json");
$arr_news_items = json_decode($news_items, "true");

foreach($arr_news_items as $key=>$news)
{
    if($news['id']==$_POST['id'])
    {
        break;
    }
}
$picture = $_POST['picture'];
array_splice($arr_news_items, $key, 1);
if(file_exists($uploads.'news-images/'.$picture))
{
    unlink($uploads.'news-images/'.$picture);
}
$news_items = json_encode($arr_news_items);

if(file_exists($json."news-front.json"))
{

    $result = file_put_contents($json."news-front.json", $news_items);
    if($result)
    {
        redirect('news-list-view.php');
    }

}
else
{
    echo "not Found!";
}