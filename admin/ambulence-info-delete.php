<?php

require_once("../config.php");

//dd($_POST);

$admin_ambulence_info_json =  file_get_contents($json."admin-ambulence-info.json");
$arr_admin_ambulence_info = json_decode($admin_ambulence_info_json, "true");

foreach($arr_admin_ambulence_info as $key=>$ambu_info)
{
    if($ambu_info['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_admin_ambulence_info, $key, 1);
$admin_ambulence_info_json = json_encode($arr_admin_ambulence_info);

if(file_exists($json."admin-ambulence-info.json"))
{

    $result = file_put_contents($json."admin-ambulence-info.json", $admin_ambulence_info_json);
    if($result)
    {
        redirect('add-ambulence-admin.php');
    }

}
else
{
    echo "Not Found!";
}
