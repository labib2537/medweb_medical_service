<?php

require_once("../config.php");

//dd($_POST);

$service_items = file_get_contents($json."service-front.json");
$arr_service_items = json_decode($service_items, "true");

foreach($arr_service_items as $key=>$service_card)
{
    if($service_card['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_service_items, $key, 1);
$service_items = json_encode($arr_service_items);

if(file_exists($json."service-front.json"))
{
    $result = file_put_contents($json."service-front.json", $service_items);
    if($result)
    {
        redirect('service-list.php');
    }
}
else{
    echo "Not Found!";
}