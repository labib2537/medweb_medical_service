<?php

require_once("../config.php");

//dd($_POST);

//dd($_FILES);

$file = null;
$new_pdf = null;
$old_pdf = null;

$old_pdf = $_POST['old_pdf'];


if(array_key_exists('new_pdf', $_FILES) && !empty($_FILES['new_pdf']['name']))
{
    $filename = uniqid().'_'.$_FILES['new_pdf']['name'];
    $from = $_FILES['new_pdf']['tmp_name'];
    $to = $uploads.'test-reports/'.$filename;
    if(upload($from, $to))
    {
        $new_pdf = $filename;
    }
    if(file_exists($uploads.'test-reports/'.$old_pdf))
    {
        unlink($uploads.'test-reports/'.$old_pdf);
    }

}


$uid = $_POST['id'];
$name = $_POST['name'];
$address = $_POST['add'];
$phone = $_POST['phone'];
$test = $_POST['test'];
$date = $_POST['date'];
$file = $new_pdf ?? $old_pdf;

$test_report = [


    "id" => $uid,
    "patient name" => $name,
    "test name" => $test,
    "address" => $address,
    "phone" => $phone,
    "date" => $date,
    "report file" => $file

];

//dd($test_report);

$test_json =  file_get_contents($json."admin-test-report.json");
$arr_test = json_decode($test_json, "true");

foreach($arr_test as $key=>$test)
{
    if($test['id']==$uid)
    {
        break;
    }
}


$arr_test[$key] = $test_report;
$test_json = json_encode($arr_test); 

if(file_exists($json."admin-test-report.json")){
    $result = file_put_contents($json."admin-test-report.json", $test_json);
}else{
    echo "Not Found!";
}

if($result)
{
    $message = 'Test Report Information is updated Successfully';
    set_session('message',$message);
    redirect('test-report-list.php');
}
