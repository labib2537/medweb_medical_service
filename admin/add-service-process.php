<?php

require_once("../config.php");



$name = $_POST['title'];
$head = $_POST['head'];
$content = $_POST['content'];
$icon = $_POST['icon'];

$services = [
        "id" => uniqid(),
        "name" => $name,
        "heading"=> $head,
        "paragraph" => $content,
        "icon"=> $icon,
];

//dd($services);


$service_items = file_get_contents($json."service-front.json");
$arr_service_items = json_decode($service_items, "true");

$arr_service_items[] = $services;
$service_items = json_encode($arr_service_items);

if(file_exists($json."service-front.json"))
{
    $result = file_put_contents($json."service-front.json", $service_items);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3">Service Card has been added Successfully.</p>
								<a href="service-list.php" class="btn bg-success-400">GO TO LIST</a>
							</div>
						</div>
    
</body>
</html>



<?php
}
else{
    "Not Found!";
}
?>
