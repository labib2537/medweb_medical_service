<?php

require_once("../config.php");

//dd($_POST);


$name = $_POST['name'];
$ambulence_no = $_POST['number'];
$location = $_POST['location'];
$phone = $_POST['phone'];
$status = "Free";
$sta_color = "badge-success";

$ambu_info = [
        "id" => uniqid(),
        "ambulence no." => $ambulence_no,
        "driver name" => $name,
        "location" => $location,
        "phone" => $phone,
        "status" => $status,
        "status color" => $sta_color
];



$admin_ambulence_info_json =  file_get_contents($json."admin-ambulence-info.json");
$arr_admin_ambulence_info = json_decode($admin_ambulence_info_json, "true");

$arr_admin_ambulence_info[] = $ambu_info;
$admin_ambulence_info_json = json_encode($arr_admin_ambulence_info);

if(file_exists($json."admin-ambulence-info.json"))
{
    $result = file_put_contents($json."admin-ambulence-info.json", $admin_ambulence_info_json);
}
else
{
    echo "Not Found!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>
<body>

<div class="card">
							<div class="card-body text-center">
							<i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
								<h5 class="card-title">Successful</h5>
								<p class="mb-3">New Ambulence has been saved Successfully.</p>
								<a href="add-ambulence-admin.php" class="btn bg-success-400">GO TO LIST</a>
							</div>
						</div>
    
</body>
</html>