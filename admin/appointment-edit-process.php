<?php

require_once("../config.php");

//dd($_POST);

$uid = $_POST['id'];
$name = $_POST['name'];
$doctor = $_POST['doc_name'];
$address = $_POST['add'];
$phone = $_POST['phone'];
$gender = $_POST['gender'];
$date = $_POST['date'];


$appointments = [

        
        "id" => $uid,
        "patient name" => $name,
        "doctor name" => $doctor,
        "gender" => $gender,
        "address" => $address,
        "phone" => $phone,
        "date" => $date

];

//dd($appointments);


$appoint_json =  file_get_contents($json."admin-appointment-list.json");
$arr_appoint = json_decode($appoint_json, "true");

foreach($arr_appoint as $key=>$appoint)
{
    if($appoint['id']==$uid)
    {
        break;
    }
}

$arr_appoint[$key] = $appointments;

$appoint_json = json_encode($arr_appoint);

if(file_exists($json."admin-appointment-list.json"))
{
    $result = file_put_contents($json."admin-appointment-list.json", $appoint_json);
}
else{
    echo "Not Found!";
}

if($result)
{
    $message = 'Appointment Information is upadated successfully';
    set_session('message', $message);
    redirect('appointment-list.php');
}