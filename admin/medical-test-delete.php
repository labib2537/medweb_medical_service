<?php

require_once("../config.php");



$test_json = file_get_contents($json.'admin-medical-test.json');
$arr_test = json_decode($test_json, "true");

foreach($arr_test as $key=>$test)
{
    if($test['id']==$_POST['id'])
    {
        break;
    }
}
$picture = $_POST['picture'];
array_splice($arr_test, $key, 1);
if(file_exists($uploads.'test-images/'.$picture))
{
    unlink($uploads.'test-images/'.$picture);
}
//dd($arr_test);
$test_json = json_encode($arr_test);

if(file_exists($json.'admin-medical-test.json'))
{
    $result = file_put_contents($json.'admin-medical-test.json', $test_json);
}
else{
    echo "Not Found!";
}

if($result)
{
    redirect('medical-test-admin.php');
}