<?php

require_once("../config.php");



$test_json = file_get_contents($json.'admin_doctor_list.json');
$arr_test = json_decode($test_json, "true");

foreach($arr_test as $key=>$test)
{
    if($test['id']==$_POST['id'])
    {
        break;
    }
}
$picture = $_POST['picture'];
array_splice($arr_test, $key, 1);
if(file_exists($uploads.'doctor-images/'.$picture))
{
    unlink($uploads.'doctor-images/'.$picture);
}
//dd($arr_test);
$test_json = json_encode($arr_test);

if(file_exists($json.'admin_doctor_list.json'))
{
    $result = file_put_contents($json.'admin_doctor_list.json', $test_json);
}
else{
    echo "Not Found!";
}

if($result)
{
    redirect('doctor-profile-admin.php');
}