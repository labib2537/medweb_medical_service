<?php

require_once("../config.php");

//dd($_POST);


$uid = $_POST['id'];
$name = $_POST['name'];
$ambulence_no = $_POST['number'];
$location = $_POST['location'];
$phone = $_POST['phone'];
$status = $_POST['status'];
$sta_color = $_POST['color'];

$ambu_info = [
        "id" => $uid,
        "ambulence no." => $ambulence_no,
        "driver name" => $name,
        "location" => $location,
        "phone" => $phone,
        "status" => $status,
        "status color" => $sta_color
];



$admin_ambulence_info_json =  file_get_contents($json."admin-ambulence-info.json");
$arr_admin_ambulence_info = json_decode($admin_ambulence_info_json, "true");

foreach($arr_admin_ambulence_info as $key=>$info)
{
    if($info['id']==$uid)
    {
        break;
    }
}


$arr_admin_ambulence_info[$key] = $ambu_info;
$admin_ambulence_info_json = json_encode($arr_admin_ambulence_info);

if(file_exists($json."admin-ambulence-info.json"))
{
    $result = file_put_contents($json."admin-ambulence-info.json", $admin_ambulence_info_json);
}
else
{
    echo "Not Found!";
}

if($result)
{
    $message = 'Ambulence Information is updated successfully';
    set_session('message',$message);
    redirect('add-ambulence-admin.php');
}