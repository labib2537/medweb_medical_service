<?php
require_once("../config.php");
//dd($_POST);
// This block is for adding new bed allotment
$bed_num = $_POST["room"];
$bed_status_json =  file_get_contents($json."admin-bed-manage.json");
$arr_bed_status_list = json_decode($bed_status_json, true);

    foreach($arr_bed_status_list as $key=>$bed)
{
    if($bed["cabin no."] == $bed_num)
    {
        break;
    }
}
$bed_stat = [  
    "cabin no." => $bed["cabin no."] ,
    "type" => $bed["type"],
    "status image" => "green.png"   
];

//dd($key);
$arr_bed_status_list[$key]  = $bed_stat;
//dd($arr_slider);
$bed_json = json_encode($arr_bed_status_list);
//dd($dataslide);
if(file_exists($json."admin-bed-manage.json")){
    $result = file_put_contents($json."admin-bed-manage.json", $bed_json);
    //echo $result;
  //give appropriate message
}
else{
    echo "Not Found!";
}

// store data using json

$name = $_POST['name'];
$age = $_POST['age'];
$phone = $_POST['phone'];
$date = $_POST['date'];
$address = $_POST['add'];
$room = $_POST['room'];
$status = "Admitted";
$sta_color = "badge-success";

$bed_allot = [

        
        "id" => uniqid(),
        "patient name" => $name,
        "address" => $address,
        "phone"  => $phone,
        "age"  => $age,
        "room" => $room,
        "date" => $date,
        "status" => $status,
        "status color" => $sta_color           
];

//dd($bed_allot);

$admin_bed_list_json =  file_get_contents($json."admin-bed-list.json");
$arr_admin_bed_list = json_decode($admin_bed_list_json);

$arr_admin_bed_list[] = (object)$bed_allot;
$admin_bed_list_json = json_encode($arr_admin_bed_list);

if(file_exists($json."admin-bed-list.json"))
{
    $result = file_put_contents($json."admin-bed-list.json", $admin_bed_list_json);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once($short.'head.php'); ?>
    <title>Successful</title>
</head>

<body>

    <div class="card">
        <div class="card-body text-center">
            <i class="icon-check2 icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
            <h5 class="card-title">Successful</h5>
            <p class="mb-3">Bed has been allocated Successfully.</p>
            <a href="bed_allotment_list.php" class="btn bg-success-400">GO TO LIST</a>
        </div>
    </div>

</body>

</html>



<?php
}
else{
    "Not Found!";
}
?>