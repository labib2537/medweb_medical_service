<?php

require_once("../config.php");

//dd($_POST);


$admin_patient_list_json =  file_get_contents($json."admin-patient-list.json");
$arr_admin_patient_list = json_decode($admin_patient_list_json, "true");

foreach($arr_admin_patient_list as $key=>$patient)
{
    if($patient['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_admin_patient_list, $key, 1);
$admin_patient_list_json = json_encode($arr_admin_patient_list);

if(file_exists($json."admin-patient-list.json"))
{

    $result = file_put_contents($json."admin-patient-list.json", $admin_patient_list_json);
    if($result)
    {
        redirect('patient-list.php');
    }

}
else
{
    echo "not Found!";
}