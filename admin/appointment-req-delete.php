<?php

require_once("../config.php");

//dd($_POST);


$admin_appoint_req_json =  file_get_contents($json."admin-appointment-req.json");
$arr_admin_appoint_req = json_decode($admin_appoint_req_json, "true");

foreach($arr_admin_appoint_req as $key=>$req)
{
    if($req['id']==$_POST['id'])
    {
        break;
    }
}

array_splice($arr_admin_appoint_req, $key, 1);
$admin_appoint_req_json = json_encode($arr_admin_appoint_req);

if(file_exists($json."admin-appointment-req.json"))
{

    $result = file_put_contents($json."admin-appointment-req.json", $admin_appoint_req_json);
    if($result)
    {
        redirect('add-appointment.php');
    }

}
else
{
    echo "not Found!";
}
