<?php

require_once("../config.php");

//dd($_POST);


$uid = $_POST['id'];
$q = $_POST['qus'];
$a = $_POST['ans'];
$d = $_POST['date'];


$ambu_info = [
        "id" => $uid,
        "question" => $q,
        "answer" => $a,
        "date" => $d
];



$admin_ambulence_info_json =  file_get_contents($json."faq.json");
$arr_admin_ambulence_info = json_decode($admin_ambulence_info_json, "true");

foreach($arr_admin_ambulence_info as $key=>$info)
{
    if($info['id']==$uid)
    {
        break;
    }
}


$arr_admin_ambulence_info[$key] = $ambu_info;
$admin_ambulence_info_json = json_encode($arr_admin_ambulence_info);

if(file_exists($json."faq.json"))
{
    $result = file_put_contents($json."faq.json", $admin_ambulence_info_json);
}
else
{
    echo "Not Found!";
}

if($result)
{
    $message = 'FAQ Information is updated successfully';
    set_session('message',$message);
    redirect('faq-management.php');
}