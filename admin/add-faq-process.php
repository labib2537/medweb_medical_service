<?php

require_once("../config.php");

//dd($_POST);

$question = $_POST['qus'];
$answer = $_POST['ans'];
$date = $_POST['date'];


$appoint_req = [
        "id" => "faq".uniqid(),
        "question" => $question,
        "answer" => $answer,
        "date" => $date
];

//dd($appoint_req);

$admin_appoint_req_json =  file_get_contents($json."faq.json");
$arr_admin_appoint_req = json_decode($admin_appoint_req_json, "true");

$arr_admin_appoint_req[] = $appoint_req;
$admin_appoint_req_json = json_encode($arr_admin_appoint_req);

if(file_exists($json."faq.json"))
{
    $result = file_put_contents($json."faq.json", $admin_appoint_req_json);
}
else{
    echo "Not Found!";
}

if($result)
{
    $message = "FAQ has been added successfully.";
    set_session('message',$message);
    redirect('faq-management.php');
}

